#include <iostream>
#include <string>

using namespace std;

/*������������ �����*/
class Animal
{
public://������ ������ ��������� �����
    virtual void Voice() {}; //�����
    virtual ~Animal() {}; 
};

/*����������� ������*/
class Sheep : public Animal //����� ����������� �� ������ "Animal" � ��������� ����� ������������
{
public:
   void Voice() override //��������������� ������
   {
       cout << "Baa!" << endl;
   }   
};

class Pig : public Animal //����� ����������� �� ������ "Animal" � ��������� ����� ������������
{
public:
    void Voice() override //��������������� ������
    {
        cout << "Oink!" << endl;
    }
};

class Cow : public Animal //����� ����������� �� ������ "Animal" � ��������� ����� ������������
{
public:
    void Voice() override //��������������� ������
    {
        cout << "Moo!" << endl;
    }
};

/*������� �������*/
int main()
{
    //������ ���������� �������
    const int size = 3; // ������ �������
    Animal* Animals[size] = { new Sheep, new Pig, new Cow }; //������� ������ (��������� ������ ��� �������)
    for (int i = 0; i < size; i++)
    {
        Animals[i]->Voice(); //����� ������
        delete Animals[i]; //������� ������
    }
    
    return 0;
}


